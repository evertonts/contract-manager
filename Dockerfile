FROM ruby:2.5.3

RUN apt-get update -y && apt-get install libnss3-dev chromium -y

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN bin/bundle
