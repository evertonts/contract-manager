# Contract Manager

Application to help people to manage their contracts. Check on Heroku https://everton-contract-manager.herokuapp.com

# Dependencies

- Ruby 2.5.3
- Postgresql

Or use Docker (prefered way)

# Running

Run

`docker-compose run web bin/rails db:setup`
`docker-compose up`

The application should be running at `http://localhost:3000`

# Running the tests

Run

`docker-compose run web bin/rspec`

## Ideas and decisions

1. When developing the app I've decided to use the Rails default in most of the cases, making it easy to an experienced Rails developer to understand and maintain.

1. In the javascript part, I also went to simplicity and used JQuery, as the only requirement that needed Javascript was loading the categories after selecting a vendor.

1. I've created a lot of acceptance tests, I usually try to use the test pyramid concept, but in this case, I've tried to do a little different using an outside In TDD.

1. For the HTML and CSS part, I've decided to not use any framework like bootstrap, mostly because I wanted to use the application to remember some concepts of CSS

## Next steps

The next steps I see for the application are:

1. Improve the layout with something more user-friendly
1. Create a way to add more categories and vendors
