require 'active_record/fixtures'

puts "\nRemoving old data..."
Vendor.delete_all
Category.delete_all
puts "\nSeeding database..."

ActiveRecord::FixtureSet.create_fixtures('spec/fixtures/', 'vendors')
ActiveRecord::FixtureSet.create_fixtures('spec/fixtures/', 'categories')
ActiveRecord::FixtureSet.create_fixtures('spec/fixtures/', 'categories_vendors')
