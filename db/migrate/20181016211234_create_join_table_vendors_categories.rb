class CreateJoinTableVendorsCategories < ActiveRecord::Migration[5.2]
  def change
    create_join_table :vendors, :categories do |t|
      t.index [:vendor_id, :category_id]
    end
  end
end
