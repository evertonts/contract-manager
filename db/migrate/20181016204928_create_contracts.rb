class CreateContracts < ActiveRecord::Migration[5.2]
  def change
    create_table :contracts do |t|
      t.references :vendor, null: false, foreign_key: true
      t.references :category, null: false, foreign_key: true
      t.decimal :costs, precision: 8, scale: 2, null: false
      t.date :ends_on, null: false
    end
  end
end
