Rails.application.routes.draw do
  root to: 'contracts#index'

  devise_for :users, path: '', path_names: { sign_up: 'signup', sign_in: 'signin' }

  resources :contracts, only: [:index, :new, :create, :update]
  get '/contracts/edit/(:id)', to: 'contracts#edit', as: :edit_contract

  resources :vendors, only: [] do
    resources :categories, only: [:index]
  end
end
