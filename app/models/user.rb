class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :validatable

  validates :full_name, presence: true

has_many :contracts
end
