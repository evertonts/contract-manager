class Contract < ApplicationRecord
  belongs_to :vendor
  belongs_to :category
  belongs_to :user

  validates :costs, numericality: { greater_than: 0, message: 'The cost value is invalid' }
  validates :ends_on, presence: { message: 'Ends On is invalid' }
  validate :ends_on_cannot_be_in_the_past

  def ends_on_cannot_be_in_the_past
    if ends_on.present? && ends_on < Date.today
      errors.add(:ends_on, "Ends On can't be in the past")
    end
  end
end
