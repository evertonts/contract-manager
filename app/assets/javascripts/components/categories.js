var categories = (function () {
  init = function() {
    this.element = $('[data-vendor]');
    this.categories = $('[data-categories]')

    this._bindEvents()
  }

  _populateCategories = function (event) {
    var selectedVendor = $(event.target).find(':selected')[0]
    var categoriesUrl = '/vendors/' + selectedVendor.value + '/categories'
    var categories = this.categories
    categories.parent().removeClass('hidden')

    $.get(categoriesUrl, function (data) {
      categories.children('option').remove();

      data.forEach(category => {
        var option = document.createElement('option');
        option.text = category.name
        option.value = category.id
        categories.append(option)
      });
    })
  }

  _bindEvents = function () {
    this.element.on('change', this._populateCategories.bind(this));
  }

  return {
    init: init,
    _bindEvents: _bindEvents,
    _populateCategories: _populateCategories
  }
})();
