class ContractsController < ApplicationController
  before_action :authenticate_user!

  def new
    @contract = Contract.new
  end

  def create
    @contract = current_user.contracts.new(contract_params)

    if @contract.save
      flash[:notice] = 'Your contract was added'
      redirect_to action: 'index'
    else
      render 'new'
    end
  end

  def index
    @contracts = current_user.contracts
  end

  def edit
    @contract = current_user.contracts.find(params[:id])
  end

  def update
    @contract = current_user.contracts.find(params[:id])

    if @contract.update(contract_params)
      flash[:notice] = 'Your contract was updated'
      redirect_to action: 'index'
    else
      render 'edit'
    end
  end

  private

  def contract_params
    params.require(:contract).permit(:vendor_id, :category_id, :costs, :ends_on)
  end
end
