class CategoriesController < ApplicationController
  before_action :authenticate_user!

  def index
    vendor = Vendor.find(params[:vendor_id])
    render json: vendor.categories
  end
end
