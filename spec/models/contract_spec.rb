require 'rails_helper'

RSpec.describe Contract do
  include ActiveSupport::Testing::TimeHelpers

  context 'validations' do
    context 'costs' do
      it 'is not valid with negative cost' do
        contract = described_class.new(costs: -1)

        expect(contract).not_to be_valid
        expect(contract.errors[:costs]).to include('The cost value is invalid')
      end

      it 'is not valid with cost 0' do
        contract = described_class.new(costs: 0.0)

        expect(contract).not_to be_valid
        expect(contract.errors[:costs]).to include('The cost value is invalid')
      end

      it 'is not valid with non numerical cost' do
        contract = described_class.new(costs: 'a,50')

        expect(contract).not_to be_valid
        expect(contract.errors[:costs]).to include('The cost value is invalid')
      end
    end

    context 'ends_on' do
      it 'is not valid with a date in the past' do
        travel_to(Date.new(2018, 1, 1)) do
          contract = described_class.new(ends_on: Date.new(2000, 1, 1))
          expect(contract).not_to be_valid
          expect(contract.errors[:ends_on]).to include('Ends On can\'t be in the past')
        end
      end
    end
  end
end
