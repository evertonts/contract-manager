require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validations' do
    it 'is not valid without a full name' do
      user = User.new(full_name: nil)

      expect(user).not_to be_valid
      expect(user.errors[:full_name]).to include('can\'t be blank')
    end

    it 'is not valid without an email' do
      user = User.new(email: nil)

      expect(user).not_to be_valid
      expect(user.errors[:email]).to include('can\'t be blank')
    end

    it 'is not valid with invalid email' do
      user = User.new(email: 'invalidemail')

      expect(user).not_to be_valid
      expect(user.errors[:email]).to include('is invalid')
    end
  end
end
