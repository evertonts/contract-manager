require 'rails_helper'

RSpec.feature 'Create contract' do
  context 'user is not logged in' do
    scenario 'redirects to the login page' do
      visit '/contracts/edit/1'

      expect(page).to have_content('You need to sign in or sign up before continuing.')
    end
  end

  context 'user is logged in' do
    let(:user) { users(:user) }
    background do
      login_as(user, :scope => :user)
    end

    scenario 'allows the contract edition when the contract belongs to the user' do
      contract = user.contracts.create(
        vendor: vendors(:vodafone),
        category: categories(:internet),
        costs: 2.5,
        ends_on: Date.tomorrow
      )

      visit "/contracts/edit/#{contract.id}"

      expect(page).to have_content('Vodafone')
      expect(page).to have_content('Internet')

      fill_in 'Costs', with: 10
      fill_in 'Ends on', with: '01/01/2020'

      click_on 'Update'

      expect(page).to have_content('Your contract was updated')
      expect(page).to have_content('$ 10')
      expect(page).to have_content('Jan 1, 2020')
    end
  end
end
