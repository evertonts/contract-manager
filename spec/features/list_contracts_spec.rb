require 'rails_helper'

RSpec.feature 'List contracts'  do
  context 'user is not logged in' do
    scenario 'redirect to login page' do
      visit '/contracts'

      expect(page).to have_content('You need to sign in or sign up before continuing.')
    end
  end

  context 'user is logged in' do
    let(:user) { user = users(:user) }

    background do
      login_as(user)
    end

    scenario 'when the user has no contracts' do
      visit '/contracts'

      expect(page).to have_content('No contracts yet')
      expect(page).to have_content('Add Contract')
    end

    scenario 'when the user has contracts' do
      vodafone = vendors(:vodafone)
      internet = categories(:internet)
      user.contracts.create(vendor: vodafone, category: internet, costs: 400, ends_on: Date.new(2020, 1, 1))

      visit '/contracts'

      expect(page).to have_content('Vodafone')
      expect(page).to have_content('Internet')
      expect(page).to have_content('$ 400.00')
      expect(page).to have_content('Jan 1, 2020')
    end
  end
end
