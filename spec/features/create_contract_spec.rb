require 'rails_helper'

RSpec.feature 'Create contract'  do
  include ActiveSupport::Testing::TimeHelpers

  context 'user is not logged in' do
    scenario 'redirects to the login page' do
      visit '/contracts/new'

      expect(page).to have_content('You need to sign in or sign up before continuing.')
    end
  end

  context 'user is logged in', js: true do
    background do
      user = users(:user)
      login_as(user, :scope => :user)
      visit '/contracts/new'
    end

    scenario 'it properly loads the vendors and categories' do
      expect(page).not_to have_content('Category')
      expect(page).to have_content('O2')
      expect(page).to have_content('Vattenfall')

      select('Vodafone', from: 'contract_vendor_id').select_option

      expect(page).to have_content('Category')
      expect(page).to have_content('Mobile Phone')

      select('Vattenfall', from: 'contract_vendor_id').select_option
      expect(page).to have_content('Electricity')
    end

    scenario 'create a new contract with valid information' do
      fill_form(vendor: 'Vodafone', category: 'Internet', ends_on: '20/01/2050', costs: 20.50)

      click_on 'Create'

      expect(page).to have_content('Your contract was added')
    end

    scenario 'with negative cost' do
      fill_form(vendor: 'Vodafone', category: 'Internet', ends_on: '20/01/2050', costs: -20.50)

      click_on 'Create'

      expect(page).to have_content('The cost value is invalid')
    end

    scenario 'with a date in the past' do
      travel_to(Date.new(2018, 1, 1)) do
        fill_form(vendor: 'Vodafone', category: 'Internet', ends_on: '01/01/2000', costs: 20.50)

        click_on 'Create'

        expect(page).to have_content('Ends On can\'t be in the past')
      end
    end

    scenario 'with an invalid date' do
      fill_form(vendor: 'Vodafone', category: 'Internet', ends_on: '01/50/2030', costs: 20.50)

      click_on 'Create'

      expect(page).to have_content('Ends On is invalid')
    end
  end

  def fill_form(vendor:, category:, ends_on:, costs:)
    select(vendor, from: 'contract_vendor_id').select_option
    assert_selector('option', text: 'Internet')
    select category, from: 'contract_category_id'
    fill_in 'Ends on', with: ends_on
    fill_in 'Costs', with: costs
  end
end
