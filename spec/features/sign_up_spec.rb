require 'rails_helper'

RSpec.feature 'sign up' do
  background { visit '/signup' }

  scenario 'user\'s signs up with valid information' do
    fill_form(full_name: 'Test User', email: 'test@example.com',
      password: 'password', password_confirmation: 'password')
  end

  scenario 'full name is empty' do
    fill_form(full_name: '', email: 'test@example.com',
      password: 'password', password_confirmation: 'password')

    click_on 'Sign up'

    expect(page).to have_content('Full name can\'t be blank')
  end

  scenario 'email is empty' do
    fill_form(full_name: 'Test User', email: '',
      password: 'password', password_confirmation: 'password')

    click_on 'Sign up'

    expect(page).to have_content('Email can\'t be blank')
  end

  scenario 'invalid email' do
    fill_form(full_name: 'Test User', email: 'john#doe.com',
      password: 'password', password_confirmation: 'password')

    click_on 'Sign up'

    expect(page).to have_content('Email is invalid')
  end

  scenario 'email has already been taken' do
    user = users(:user)
    user.update(email: 'existing@mail.com')

    fill_form(full_name: 'Test User', email: 'existing@mail.com',
      password: 'password', password_confirmation: 'password')

    click_on 'Sign up'

    expect(page).to have_content('This Email is already taken')
  end

  scenario 'empty password' do
    fill_form(full_name: 'Test User', email: 'test@example.com',
      password: '', password_confirmation: 'password')

    click_on 'Sign up'

    expect(page).to have_content('Password can\'t be blank')
  end

  scenario 'password is too short' do
    fill_form(full_name: 'Test User', email: 'test@example.com',
      password: '1234567', password_confirmation: '1234567')

    click_on 'Sign up'

    expect(page).to have_content('The Password is too short')
  end

  scenario 'password confirmation does\'t match' do
    fill_form(full_name: 'Test User', email: 'test@example.com',
      password: 'password', password_confirmation: '1234567')

    click_on 'Sign up'

    expect(page).to have_content('The Password confirmation doesn\'t match')
  end

  def fill_form(full_name:, email:, password:, password_confirmation:)
    fill_in 'Full name', with: full_name
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    fill_in 'Password confirmation', with: password_confirmation
  end
end
