require 'rails_helper'

RSpec.feature 'sign in' do
  background do
    User.create(full_name: 'John Doe', email: 'john.doe@gmail.com', password: '12345678')
    visit '/signin'
  end

  scenario 'Fill form with valid information' do
    fill_form(email: 'john.doe@gmail.com', password: '12345678')
  end

  scenario 'Email doesn\'t match' do
    fill_form(email: 'john.doe@outlook.com', password: '12345678')

    click_on('Sign In')

    expect(page).to have_content('We weren\'t able to find a user with the specified Email and password combination')
  end

  scenario 'Password doesn\'t match' do
    fill_form(email: 'john.doe@gmail.com', password: '87654321')

    click_on('Sign In')

    expect(page).to have_content('We weren\'t able to find a user with the specified Email and password combination')
  end

  def fill_form(email:, password:)
    fill_in 'Email', with: email
    fill_in 'Password', with: password
  end
end
